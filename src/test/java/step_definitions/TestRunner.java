package step_definitions;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature-files/scenarios.feature", glue = {"checkout_tests"},
        monochrome = true,
        plugin = {"pretty", "junit:target/JUnitReports.xml","json:target/JSONReports","html:target/HTMLReports"}
)
public class TestRunner {

}
