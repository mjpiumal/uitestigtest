package checkout_tests;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


public class TestingTest {


    private WebDriver driver;

    @Before
    public void browser_is_open() {

        WebDriverManager.chromedriver().setup();

        //Create Chrome Options
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        options.addArguments("--headless");
        options.addArguments("--test-type");
        options.addArguments("--disable-popup-bloacking");
        options.addArguments("disable-infobars");
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("blink-settings=imagesEnabled=false");
        options.addArguments("--disable-gpu");
        options.addArguments("--add-host host.docker.internal:host-gateway");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-web-security");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-notifications");
        options.addArguments("--remote-debugging-port=9222");
        options.addArguments("--disable-extensions");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--allow-insecure-localhost");
        options.addArguments("--window-size=1024,768");
        System.setProperty("webdriver.chrome.args", "--disable-logging");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        options.setPageLoadStrategy(PageLoadStrategy.NONE);
        options.setExperimentalOption("useAutomationExtension", false);
//
//        DesiredCapabilities chrome = DesiredCapabilities.chrome();
//        chrome.setJavascriptEnabled(true);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        options.setCapability(ChromeOptions.CAPABILITY, options);


//        DesiredCapabilities chrome = DesiredCapabilities.chrome();
//        chrome.setJavascriptEnabled(true);
//        options.setCapability(ChromeOptions.CAPABILITY, options);

        //Create driver object for Chrome
         driver = new ChromeDriver(options);

    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Given("user is on login page")
    public void user_is_on_login_page() throws InterruptedException {
        driver.get("https://opensource-demo.orangehrmlive.com/");
//        driver.get("http://172.23.0.1:6868/signin");
        driver.manage().window().maximize();
        System.out.println("session started");
        Thread.sleep(15000);
        System.out.println(driver.getTitle());
        Thread.sleep(5000);
    }

}
